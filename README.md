ansible_role_nxlog
=========

An Ansible role to install and configure NXLog on a Windows host to forward Security logs to a Graylog server

Requirements
------------


Role Variables
--------------
Defaults:
| Variable | Default Value |
| ------ | ------ |
| nxlog_download_dest | C:\Users\{{ ansible_ssh_user }}\Downloads\nxlog-ce-{{ nxlog_download_version }}.msi |
| nxlog_download_url | https://nxlog.co/system/files/products/files/348/nxlog-ce-{{ nxlog_download_version }}.msi  |
| nxlog_graylog_port | 12201 |

Additional required vars:    
nxlog_graylog_server: yourgraylogserverip
nxlog_download_version: version # to download (such as 2.11.2190)

Dependencies
------------


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Install and Configure NXLog
      hosts: servers
      roles:
         - ansible_role_nxlog
      vars:
        nxlog_graylog_server: 192.168.0.10

License
-------

MIT

Author Information
------------------

John Potter, DREAM Team
